// Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3) 

let num = prompt(`Enter a number:`)
let number = num
let getCube = number ** 3

// Using template Literals, print out the value of the getCube variable with a message of The cube of <num> is..
let result = `The cube of ${num} is ${getCube}`
console.log(result)

// Create a variable address with a value of an array containing details of an address.

const address = [" Blk. 4 Lot 18 Ph 8, Santol Street ", "Carmona Estates", "Carmona Cavite"]

// Destructure the array and print out a message with the full address using template Literals.

const [streetNum, subdName, cityName] = address

console.log(`I live at ${streetNum} ${subdName} ${cityName} .`)

// Create a variable animal with a value of an object data type with different animal details as it's properties.

const animal = {
  Name: "Lolong",
  Type: "saltwater crocodile",
  Weight: "1075 kgs",
  Height: "20ft 3 in"
}

// Destructure the object and print out a message with the details of the animal using Template Literals.

const {Name, Type, Weight, Height} = animal

console.log(`${Name} was a ${Type}. He weighed at ${Weight} with a measurement of ${Height}.`)

//Create an array of numbers.
const numbers = [1, 2, 3, 4, 5]

// Loop through the array using forEach, an arrow function and using the implicit return statement to pring out the numbers.

numbers.forEach((numbers) => console.log(`${numbers}`))

// Create a class of a Dog and a constructor that will accept a name, age and breed as it's properties.

class Dog {
    constructor(name, age, breed) {
      this.name = name,
      this.age = age,
      this.breed = breed
    }
}

// Create/instantiate a new object from the class Dog and console log the object.

const myDog = new Dog()

myDog.name = "Barawneee"
myDog.age = "2yrs old"
myDog.breed = "Shitzu/Havanese"

console.log(myDog)